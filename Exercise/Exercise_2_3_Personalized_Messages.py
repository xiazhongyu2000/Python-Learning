# 练习2-3：个性化消息　用变量表示一个人的名字，并向其显示一条消息。显示的消息应非常简单，下面是一个例子。
# Hello Eric, would you like to learn some Python today?
name = "Eric"
message = f"Hello {name},would you like to learn some Python today?"
print(message)
