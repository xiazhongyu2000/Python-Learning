# 练习4-12：使用多个循环　在本节中，为节省篇幅，程序foods.py的每个版本都没有使用for循环来打印列表。
# 请选择一个版本的foods.py，在其中编写两个for循环，将各个食品列表打印出来。
my_foods=[' pizza',' falafel',' carrot cake']
for my_food in my_foods:
    print(my_food)
for my_food in my_foods[:]:
    print(my_food)
