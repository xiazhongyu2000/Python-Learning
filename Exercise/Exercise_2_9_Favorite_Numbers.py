# 练习2-9：最喜欢的数　用一个变量来表示你最喜欢的数，再使用这个变量创建一条消息，指出你最喜欢的数是什么，然后将这条消息打印出来。
fav_num = 42
msg = f"My Favorite Number is {fav_num}."
print(msg)
