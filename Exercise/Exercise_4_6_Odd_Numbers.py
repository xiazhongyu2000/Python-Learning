# 练习4-6：奇数　通过给函数range()指定第三个参数来创建一个列表，其中包含1～20的奇数，再使用一个for循环将这些数打印出来。
odd_numbers=(list(range(1,21,2)))
for odd_number in odd_numbers:
    print(odd_number)
