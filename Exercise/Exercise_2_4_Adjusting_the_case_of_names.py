# 练习2-4：调整名字的大小写　用变量表示一个人的名字，再以小写、大写和首字母大写的方式显示这个人名。
name = "Xia Zhongyu"
print(name.lower())
print(name.upper())
print(name.title())
