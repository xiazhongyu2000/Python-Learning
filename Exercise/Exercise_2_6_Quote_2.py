# 练习2-6：名言2　重复练习2-5，但用变量famous_person表示名人的姓名，再创建要显示的消息并将其赋给变量message，然后打印这条消息。
famous_person = "Albert Einstein"
message = f"{famous_person} once said, “A person who never made amistake never tried anything new.”"
print(message)
