# 练习3-4：嘉宾名单　如果你可以邀请任何人一起共进晚餐（无论是在世的还是故去的），你会邀请哪些人？
# 请创建一个列表，其中包含至少三个你想邀请的人，然后使用这个列表打印消息，邀请这些人来与你共进晚餐。
guests = ['guido van rossum', 'jack turner', 'lynn hill']
name = guests[0].title()
print(f"{name},please come to dinner.")
name = guests[1].title()
print(f"{name},please come to dinner.")
name = guests[2].title()
print(f"{name},please come to dinner.")
