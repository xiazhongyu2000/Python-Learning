# 练习3-3：自己的列表　想想你喜欢的通勤方式，如骑摩托车或开汽车，并创建一个包含多种通勤方式的列表。根据该列表打印一系列有关这些通勤方式的宣言，下面是一个例子。
# I would like to own a Honda motorcycle.
commutes = ['Self-driving', 'public transportation', 'taxi', 'cycling']
msg = f"{commutes[0]} is the fastest and most comfortable way to commute."
print(msg)
msg = f"{commutes[1]} is the greenest way to commute."
print(msg)
