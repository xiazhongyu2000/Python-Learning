# 练习3-10：尝试使用各个函数　想想可存储到列表中的东西，如山川、河流、国家、城市、语言或你喜欢的任何东西。
# 编写一个程序，在其中创建一个包含这些元素的列表，然后，对于本章介绍的每个函数，都至少使用一次来处理这个列表。
everythings = ['xia', 'zhong', 'yu']
everythings.append('sun')
print(everythings)
everythings.insert(0, 'universe')
print(everythings)
del everythings[1]
print(everythings)
everythings.pop()
print(everythings)
everythings.remove('zhong')
print(everythings)
everythings.sort(reverse=True)
print(everythings)
print(len(everythings))
