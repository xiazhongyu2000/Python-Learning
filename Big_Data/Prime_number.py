# 3、编写程序，输出200以内最大的素数。
def is_prime(n):
    if (n <= 1):
        return False
    for i in range(2, n):
        if n % i == 0:
            return False
        return True


def find_prime(n):
    for i in range(n, 0, -1):
        if is_prime(i):
            return i


print(find_prime(200))
