# 4、编写程序，输出各位数字都不相同的所有三位数。
count = 0
for i in range(1, 10):
    for j in range(1, 10):
        for k in range(1, 10):
            if i != k and i != j and j != k:  # 判断三个数是否互不相同
                count += 1
                print("%d%d%d  " % (i, j, k), end=" ")
                if count % 10 == 0:
                    print()
print("\n三位互不相同的数，一共有：{} 个。".format(count))
