# 2、首先生成[1,100]区间上的一个随机数，然后根据随机数的范围生成变量sex的值，当随机数大于51时把sex设置为男，否则设置为女。
import random

x = random.choice(range(1, 101))
if x > 51:
    sex = '男'
else:
    sex = '女'
print(sex)
